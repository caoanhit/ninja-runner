﻿Shader "VertexColor"
{
	Properties
	{
		_Color("Color", Color) = (0,0,0,0)
		[NoScaleOffset] _Gradient("Gradient", 2D) = "white" {}
	}
	SubShader
	{
		Cull off
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
				fixed4 color : COLOR;
			};

			struct v2f
			{
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				fixed4 vcolor : COLOR0;
				fixed3 color : COLOR1;

			};
			fixed4 _Color;
			sampler2D _Gradient;
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.vcolor = v.color+_Color;
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				o.color = max(0.005, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_Gradient,i.color)* i.vcolor;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}
