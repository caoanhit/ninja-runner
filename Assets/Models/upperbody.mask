%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: upperbody
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Bone
    m_Weight: 0
  - m_Path: Armature/Torso
    m_Weight: 0
  - m_Path: Armature/Torso/Belly
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Neck
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Neck/Scarf1
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Neck/Scarf1/Scarf2
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Neck/Scarf1/Scarf2/Scarf3
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Neck/Scarf1/Scarf2/Scarf3/Scarf4
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Finger_1_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Finger_1_l/Finger_2_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Finger_1_l/Finger_2_l/Finger_3_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Thumb_1_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Thumb_1_l/Thumb_2_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Thumb_1_l/Thumb_2_l/Thumb_3_l
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Finger_1_r
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Finger_1_r/Finger_2_r
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Finger_1_r/Finger_2_r/Finger_3_r
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Thumb_1_r
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Thumb_1_r/Thumb_2_r
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Thumb_1_r/Thumb_2_r/Thumb_3_r
    m_Weight: 1
  - m_Path: Armature/Torso/Thigh_l
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_l/Leg_l
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_l/Leg_l/Foot_l
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_l/Leg_l/Foot_l/Toe_l
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_r
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_r/Leg_r
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_r/Leg_r/Foot_r
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_r/Leg_r/Foot_r/Toe_r
    m_Weight: 0
  - m_Path: Cube
    m_Weight: 0
  - m_Path: Cube_003
    m_Weight: 0
  - m_Path: Cube_004
    m_Weight: 0
