﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {
	[System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }
    #region Singleton
    public static ObjectPool Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;
    List<GameObject> activeobjects;
    int objectCount=0;
	void Start () {
        activeobjects = new List<GameObject>();
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach(Pool pool in pools)
        {
            objectCount += pool.size;
            Queue<GameObject> objectsPool = new Queue<GameObject>();
            for (int i=0; i<pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                objectsPool.Enqueue(obj);
                obj.SetActive(false); 
            }
            poolDictionary.Add(pool.tag, objectsPool);
        }
	}
	public void SpawnFromPool (string tag, Vector3 position, Quaternion rotation)
    {
        GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;
        if (activeobjects.Count < objectCount)
            activeobjects.Add(objectToSpawn);
        poolDictionary[tag].Enqueue(objectToSpawn);
    }
    public void Restart()
    {
        int count = activeobjects.Count;
        for (int i=0; i<count ; i++)
        {
            activeobjects[i].SetActive(false);
        }
    }
	// Update is called once per frame
	
}
