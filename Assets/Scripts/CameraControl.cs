﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
    #region Singleton
    public static CameraControl Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    public Vector3 startpos, playpos, startoffset, playoffset;
    public float readyspeed;
    public float movespeed;
    public Transform target;
    public float shaketime;
    public Vector3 shakeamount;
    public float zoomspeed;
    public float range;
    public float smoothtime = 0.6f;

    private Vector3 shakeoffset;
    private Vector3 currentpos;
    private Vector3 currentoffset;
    private Vector3 targetpos;
    private Vector3 destiny;
    private Vector3 velo =Vector3.zero;
    private Camera cam;
    private bool start=false;
	// Use this for initialization
	void Start () {
        cam = Camera.main;
        currentoffset = startoffset;
        destiny = startpos;
        currentpos = startpos;
        transform.position = startpos + Vector3.forward;
        transform.rotation = Quaternion.LookRotation(startoffset - transform.position +Vector3.forward);
	}

    // Update is called once per frame
    void LateUpdate()
    {
        if (start)
        {
            targetpos = new Vector3(target.position.x * (range / (range + Mathf.Abs(target.position.x))), 0, Mathf.Max(target.position.z, targetpos.z));
            targetpos += Vector3.forward * movespeed * Time.deltaTime;
            destiny = Vector3.SmoothDamp(destiny, targetpos + currentpos, ref velo, smoothtime);
            transform.position = destiny + shakeoffset;
        }
    }
    public void StartGame(float readytime)
    {
        StartCoroutine(GetReady(readytime));
    }
    IEnumerator GetReady(float time)
    {
        float mul = Vector3.Distance(startpos, playpos) / Vector3.Distance(startoffset, playoffset);
        targetpos = new Vector3(target.position.x * (range / (range + Mathf.Abs(target.position.x))), 0, Mathf.Max(target.position.z, targetpos.z));
        float timepassed = 0;
        while (timepassed < time)
        {
            if (cam.fieldOfView < 90)
            {
                cam.fieldOfView += zoomspeed * Time.deltaTime;
            }
            timepassed += Time.deltaTime;
            currentpos = Vector3.MoveTowards(currentpos, playpos, readyspeed * Time.deltaTime);
            currentoffset = Vector3.MoveTowards(currentoffset, playoffset, readyspeed/mul*Time.deltaTime);
            destiny = targetpos + currentpos + Vector3.forward;
            transform.position = destiny;
            transform.rotation = Quaternion.LookRotation((targetpos + currentoffset) - (transform.position - Vector3.forward));
            yield return null;
        }
        
        
    }
    IEnumerator Shake(float mul)
    {
        float timepassed = 0;
        while (timepassed < shaketime)
        {
            timepassed += Time.unscaledDeltaTime;
            shakeoffset = new Vector3(Random.Range(shakeamount.x, -shakeamount.x), Random.Range(shakeamount.y, -shakeamount.y), Random.Range(shakeamount.z, -shakeamount.z));
            shakeoffset *= mul;
            yield return null;
        }
        shakeoffset = Vector3.zero;
    }
    public void CameraShake(float mul)
    {
        StartCoroutine(Shake(mul));
    }
    public void StartMoving()
    {
        start = true;
    }
    public void Restart()
    {
        start = false;
        transform.position = playpos+Vector3.forward;
        targetpos = Vector3.zero+Vector3.forward;
        destiny = playpos+Vector3.forward;
        transform.rotation = Quaternion.LookRotation(playoffset - transform.position+Vector3.forward);
    }
}
