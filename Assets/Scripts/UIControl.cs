﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIControl : MonoBehaviour {
    #region Singleton
    public static UIControl Instance;
    private void Awake()
    {
        TransitionLayer.SetActive(true);
        Instance = this;
    }
    #endregion
    public GameObject TransitionLayer;
    public PopupMenu MainMenu, PauseMenu, PlayMenu, ExitMenu, DeathMenu, AchivementMenu, StoreMenu, SettingMenu,LeaderBoardMenu;
    [HideInInspector]
    public bool paused=false;
    
    Animator transitionAnim;
    List<PopupMenu> OpenedMenu;
    bool playing=false;
    // Use this for initialization
    void Start () {
        Time.timeScale = 1;
        OpenedMenu = new List<PopupMenu>();
        transitionAnim = TransitionLayer.GetComponent<Animator>();
	}
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.Menu))
        {
            if (Time.timeScale > 0) Open(PauseMenu);
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if(OpenedMenu.Count!=0)
            {
                Close();
            }           
            else Open(ExitMenu);
        }
        
	}
    public void StartGame(float readytime)
    {
        MainMenu.FadeOut();
        MainMenu.Disable();
        PlayMenu.FadeIn();
    }
    public void Pause()
    {
        Open(PauseMenu);
    }
    void Open(PopupMenu menu)
    {
        paused = true;
        InputControl.Instance.enabled = false;
        Time.timeScale = 0;
        menu.Popup();
        OpenedMenu.Add(menu);
    }
    public void Close()
    {
        LineDrawer.Instance.StopDraw();
        OpenedMenu[OpenedMenu.Count - 1].Popout();
        OpenedMenu.RemoveAt(OpenedMenu.Count - 1);
        if (OpenedMenu.Count == 0)
        {
            paused = false;
            Time.timeScale = 1;
            InputControl.Instance.enabled = playing;
        }
    }
    public void End()
    {
        PlayMenu.FadeOut();
    }
    public void Death()
    {
        Open(DeathMenu);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void ExitConfirm()
    {
        Open(ExitMenu);
    }
    public void Setting()
    {
        Open(SettingMenu);
    }
    public void Shop()
    {
        Open(StoreMenu);
    }
    public void Mainmenu()
    {
        StartCoroutine(ReloadSceen());
    }
    IEnumerator ReloadSceen()
    {
        Transition();
        yield return new WaitForSecondsRealtime(0.25f);
        Scene currentscene = SceneManager.GetActiveScene();
        SceneManager.LoadSceneAsync(currentscene.name);
    }
    public void Transition()
    {
        transitionAnim.SetTrigger("Transition");
    }
    public void Playing()
    {
        playing = true;
    }
    public void Restart()
    {
        PlayMenu.FadeIn();
    }
}
