﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboCounter : MonoBehaviour {
    #region Singleton
    public static ComboCounter Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    public float timmer = 3;

    Text comboText;
    public int count;
    public int max;
    Animator anim;
	// Use this for initialization
	void Start () {
        comboText = GetComponent<Text>();
        anim = GetComponent<Animator>();
	}
    public void Count()
    {
        StopAllCoroutines();
        StartCoroutine(Counter());
    }
    IEnumerator Counter()
    {
        count++;
        comboText.text = "x" + count.ToString();
        if(count>1)
            anim.SetTrigger("Count");
        if (count > max) max = count;
        yield return new WaitForSeconds(timmer);
        count = 0;
    }
    public void Restart()
    {
        count = 0;
        max = 0;
    }
    public int MaxCombo()
    {
        return max;
    }
}
