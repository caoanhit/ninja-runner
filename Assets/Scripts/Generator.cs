﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour {
    #region Singleton
    public static Generator Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    public Vector2 RandomRange;
    public float FoliagePos;
    public float Step;
    public float Offset;
    public Transform character;
    ObjectPool objectpool;
    List<string> tags;
    float currentpos=6;
    float groundpos = -6;
	// Use this for initialization
	void Start () {
        tags = new List<string>();
        objectpool = ObjectPool.Instance;
        for (int i=0; i<3; i++)
        {
            objectpool.SpawnFromPool("ground", new Vector3(0, 0, groundpos),Quaternion.identity);
            groundpos += 12;
        }
        for (int i=0; i<objectpool.pools.Count; i++)
        {
            tags.Add(objectpool.pools[i].tag);
        }
        while (currentpos < Offset)
        {
            SpawnFoliage(-FoliagePos - 8);
            SpawnFoliage(-FoliagePos-4);
            SpawnFoliage(FoliagePos);
            SpawnFoliage(-FoliagePos);
            currentpos += Step;
        }
	}
	// Update is called once per frame
	void Update () {
		if (character.position.z > currentpos-Offset)
        {
            SpawnFoliage(FoliagePos);
            SpawnFoliage(-FoliagePos);
            currentpos += Step;
        }
        if (character.position.z > groundpos - 16)
        {
            objectpool.SpawnFromPool("ground", new Vector3(0, 0, groundpos), Quaternion.identity);
            groundpos += 12;
        }
    }
    void RandomSpawn(Vector3 position, Vector2 range,int minIndex, int maxIndex)
    {
        Vector3 offset = new Vector3(Random.Range(range.x, -range.x), 0.2f,Random.Range(range.y, -range.y));
        Quaternion rotation = Quaternion.identity;
        rotation.eulerAngles =new Vector3 (-90, Random.Range(0, 360), 0);
        objectpool.SpawnFromPool(tags[Random.Range(minIndex, maxIndex)],position+offset,rotation);
    }
    void SpawnFoliage(float position)
    {
        RandomSpawn(new Vector3(position, 0, currentpos), RandomRange, 1, 5);
    }
    void SpawnObstacle()
    {

    }
    public void Restart()
    {
        objectpool.Restart();
        currentpos = 0;
        while (currentpos < Offset)
        {
            SpawnFoliage(-FoliagePos - 8);
            SpawnFoliage(-FoliagePos - 4);
            SpawnFoliage(FoliagePos);
            SpawnFoliage(-FoliagePos);
            currentpos += Step;
        }
        groundpos = -6;
        for (int i = 0; i < 3; i++)
        {
            objectpool.SpawnFromPool("ground", new Vector3(0, 0, groundpos), Quaternion.identity);
            groundpos += 12;
        }
    }
}