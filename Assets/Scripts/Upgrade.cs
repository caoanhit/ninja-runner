﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Upgrade : ScriptableObject {
    string upgradeName;
    int maxLevel;
    List<float> values;
}
