﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {
    #region Singleton
    public static Character Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    public float dashforce;
    public float knockbackforce;
    public float minspeed;
    public float maxforce;
    public Transform slash;
    public Transform attack1, attack2, attack3;
    public ParticleSystem deathparticle, dashparticles, hitparticle;
    public List<GameObject> mesh;
    public GameObject line;
    Rigidbody rb;
    Animator anim;
    ParticleSystem slashfx;
    Vector3 direction;
    bool stunned=false;
    bool DashAvailable=true;
    Material material;
    // Use this for initialization
    void Start () {
        material = GetComponent<Material>();
        slashfx = slash.GetComponent<ParticleSystem>();
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
	}
	// Update is called once per frame
	void Update () {
        anim.SetFloat("Speed", rb.velocity.magnitude);
        if (rb.velocity.magnitude < minspeed)
        {
            rb.velocity = Vector3.zero;
        }
    }
    public void Ready()
    {
        anim.SetTrigger("Start");
    }
    public void Move(Vector3 dir)
    {
        if (!stunned&& DashAvailable)
        {
            anim.SetTrigger("Dash");
            CameraControl.Instance.StartMoving();
            rb.velocity = Vector3.zero;
            direction = dir.normalized;
            transform.rotation = Quaternion.LookRotation(direction);
            rb.AddForce(direction * Mathf.Min((dashforce * dir.magnitude), maxforce), ForceMode.Impulse);
            var emitParams = new ParticleSystem.EmitParams();
            emitParams.position = transform.position + transform.forward * 1f + Vector3.up * 0.5f;
            emitParams.rotation3D = transform.rotation.eulerAngles;
            dashparticles.Emit(emitParams, 1);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "obstacle")
        {
            hitparticle.Play();
            anim.SetTrigger("Stunned");
            rb.velocity = Vector3.zero;
            rb.AddForce(-direction * knockbackforce, ForceMode.Impulse);
            CameraControl.Instance.CameraShake(2);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.gameObject.tag == "enemy")
            Attack(other.gameObject.transform.position);
    }
    public void Restart()
    {
        foreach (GameObject m in mesh) m.SetActive(true);
        transform.position = Vector3.zero;
        transform.forward = Vector3.forward;
        rb.velocity = Vector3.zero;
        InputControl.Instance.enabled = true;
    }
    void Attack(Vector3 target)
    {
        //StartCoroutine(SlowMotion());
        Vector3 targetdir = target - transform.position;
        float angledir = AngleDir(transform.forward, targetdir, transform.up);
        if (angledir > 0.6f) Attack1();
        else if (angledir < -0.6f) Attack2();
        else Attack3();
    }
    float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);
        return dir;
    }
    void Attack1()
    {
        anim.SetTrigger("Attack1");
        var emitParams = new ParticleSystem.EmitParams();
        emitParams.position = attack1.position;
        emitParams.rotation3D = attack1.rotation.eulerAngles;
        slashfx.Emit(emitParams,1);
    }
    void Attack2()
    {
        anim.SetTrigger("Attack2");
        var emitParams = new ParticleSystem.EmitParams();
        emitParams.position = attack2.position;
        emitParams.rotation3D = attack2.rotation.eulerAngles;
        slashfx.Emit(emitParams, 1);
    }
    void Attack3()
    {
        anim.SetTrigger("Attack3");
        var emitParams = new ParticleSystem.EmitParams();
        emitParams.position = attack3.position;
        emitParams.rotation3D = attack3.rotation.eulerAngles;
        slashfx.Emit(emitParams, 1);
    }
    public void Die()
    {
        UIControl.Instance.End();
        StartCoroutine(DieCoroutine());
    }
    IEnumerator Flickering(int time, float delay)
    {
        for (int i=0; i< time; i++)
        {
            foreach (GameObject m in mesh) m.SetActive(false);
            yield return new WaitForSeconds(delay / 2);
            foreach (GameObject m in mesh) m.SetActive(true);
            yield return new WaitForSeconds(delay);
        }
    }
    IEnumerator DieCoroutine()
    {
        Time.timeScale = 0;
        ScoreManager.Instance.Summary();
        InputControl.Instance.enabled = false;
        yield return new WaitForSecondsRealtime(0.3f);
        foreach (GameObject m in mesh) m.SetActive(false);
        deathparticle.gameObject.transform.position = transform.position + Vector3.up;
        deathparticle.Emit(20);
        yield return new WaitForSecondsRealtime(1);
        UIControl.Instance.Death();
    }
    IEnumerator SlowMotion()
    {
        Time.timeScale = 0.7f;
        yield return new WaitForSeconds(0.1f);
        if (!UIControl.Instance.paused)
            Time.timeScale = 1;
    }

}
