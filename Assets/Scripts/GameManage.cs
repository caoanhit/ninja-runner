﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManage : MonoBehaviour {
    public float readytime;
    #region Singleton
    public static GameManage Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    public Character character;
	// Use this for initialization
	void Start () {     
        Application.targetFrameRate = 60;
        InputControl.Instance.enabled = false;
	}
	
    public void GameStart()
    {
        StartCoroutine(GetReady());
    }
    IEnumerator GetReady()
    {
        UIControl.Instance.StartGame(readytime);
        Character.Instance.Ready();
        yield return new WaitForSeconds(0.1f);
        CameraControl.Instance.StartGame(readytime);
        yield return new WaitForSeconds(readytime);
        InputControl.Instance.enabled = true;
        UIControl.Instance.Playing();
    }
    void GamePause()
    {

    }
    public void Restart()
    {
        StartCoroutine(RestartGame());
    }
    IEnumerator RestartGame()
    {
        UIControl.Instance.Transition();
        yield return new WaitForSecondsRealtime(0.25f);
        UIControl.Instance.Close();
        UIControl.Instance.Restart();
        character.Restart();
        LineDrawer.Instance.StopDraw();
        Generator.Instance.Restart();
        CameraControl.Instance.Restart();
        ScoreManager.Instance.Reset();
        ComboCounter.Instance.Restart();
    }
}
