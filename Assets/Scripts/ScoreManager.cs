﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
    #region Singleton
    public static ScoreManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    public Transform character;
    public Text Score;
    public Text sumScore;
    public Text highcoreText;
    public Text sumhighScoreText;
    public Text coinText;

    public int point=0;
    public int kill=0;
    public int coin = 0;
    public List<int> enemytypes;

    int highscore=0;
    int AdditionPoint;
    int DistancePoint;
    GameSave save;
    // Use this for initialization
    void Start () {
        save = SaveGame.Instance.LoadData();
        highscore = save.info[0];
        highcoreText.text = highscore.ToString();
        AdditionPoint = DistancePoint = 0;
	}
	
	// Update is called once per frame
	void Update () {
        int distance = (int)character.position.z/4;
        DistancePoint = Mathf.Max(DistancePoint, distance);
        point = DistancePoint + AdditionPoint;
        Score.text = (point).ToString();
        if (point > highscore) highscore = point;
	}
    public void KillPoint(int point, int id)
    {
        AdditionPoint += point+ComboCounter.Instance.count;
        kill++;
        bool killed = false;
        foreach (int i in enemytypes)
        {
            if (i == id) killed = true;
        }
        if (!killed) enemytypes.Add(id);
    }
    public void AddCoin()
    {
        coin++;
        coinText.text = (save.info[1] + coin).ToString();
    }
    public void Reset()
    {
        AdditionPoint = DistancePoint = 0;
        point=kill =coin= 0;
    }
    public float CurrentPoint()
    {
        return DistancePoint + AdditionPoint;
    }
    public void Summary()
    {
        sumScore.text = point.ToString();
        sumhighScoreText.text = highscore.ToString();
        coinText.text = coin.ToString();
        save.info[0] = highscore;
        save.info[1] = save.info[1] + coin;
        save.info[2] = save.info[2] + kill;
        SaveGame.Instance.SaveData(save);
    }
}
