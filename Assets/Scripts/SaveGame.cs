﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public class SaveGame : MonoBehaviour
{
    public static SaveGame Instance;
    private void Awake()
    {
        Instance = this;
    }

    public void SaveData(GameSave info) {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/player.sav", FileMode.Create);
        bf.Serialize(stream, info);
        stream.Close();
    }
    public GameSave LoadData()
    {
        GameSave save=new GameSave();
        if (File.Exists(Application.persistentDataPath + "/player.sav"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/player.sav", FileMode.Open);
            save = bf.Deserialize(stream) as GameSave;
            stream.Close();
        }
        return save;
    }
}
[Serializable]
public class GameSave {
    public int[] info;
    public int[] missionLevel;
    public bool[] completedmission;
    public int[] activemission;
    public int[] potioncount;
    public GameSave()
    {
        info = new int[3];
        potioncount = new int[3];
    }
}
