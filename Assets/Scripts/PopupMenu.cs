﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupMenu : MonoBehaviour {
    public float AnimationSpeed=8;
    public bool Enable=false;

    GameObject window;
    Image layer;
    CanvasGroup canvasGroup;
	// Use this for initialization
	void Start () {
        layer = GetComponent<Image>();
        window = transform.GetChild(0).gameObject;
        if (!Enable)
        {
            if (layer != null)
                layer.enabled = false;
            window.SetActive(false);
        }
        canvasGroup = GetComponent<CanvasGroup>();
        if (!Enable) canvasGroup.blocksRaycasts = false;
    }
	
	// Update is called once per frame
	public void Popup()
    {
        StartCoroutine(popupCoroutine());
    }
    public void Popout()
    {
        StartCoroutine(popoutCoroutine());
    }
    public void FadeIn()
    {
        StartCoroutine(fadeinCoroutine());
    }
    public void FadeOut()
    {
        StartCoroutine(fadeoutCoroutine());
    }
    IEnumerator popupCoroutine()
    {
        if(layer!=null)
        layer.enabled = true;
        window.SetActive(true);
        float size = 0;
        while (size < 1)
        {
            size += AnimationSpeed * Time.unscaledDeltaTime;
            window.transform.localScale = Vector3.one *(0.875f+ size / 8); 
            canvasGroup.alpha = size;
            yield return null;
        }
        canvasGroup.blocksRaycasts = true;
    }
    IEnumerator popoutCoroutine()
    {
        canvasGroup.blocksRaycasts = false;
        float size = 1;
        while (size > 0)
        {
            size -= AnimationSpeed * Time.unscaledDeltaTime;
            window.transform.localScale = Vector3.one * (size / 8 + 0.875f);
            canvasGroup.alpha = size;
            yield return null;
        }
        if (layer != null)
            layer.enabled = false;
        window.SetActive(false);
    }
    IEnumerator fadeinCoroutine()
    {
        if (layer != null)
            layer.enabled = true;
        window.SetActive(true);
        float alpha = 0;
        while (alpha <1)
        {
            alpha += AnimationSpeed * Time.unscaledDeltaTime;
            canvasGroup.alpha = alpha;
            yield return null;
        }
        canvasGroup.blocksRaycasts = true;
    }
    IEnumerator fadeoutCoroutine()
    {
        float alpha = 1;
        while (alpha > 0)
        {
            alpha -= AnimationSpeed * Time.unscaledDeltaTime;
            canvasGroup.alpha = alpha;
            yield return null;
        }
        if (layer != null)
            layer.enabled = false;
        window.SetActive(false);
    }
    public void Disable()
    {
        canvasGroup.interactable = false;
    }
}
