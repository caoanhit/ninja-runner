﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {
    public Transform target;
    public Vector3 offset;
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3 (0,0,target.position.z) + offset;
	}
}
