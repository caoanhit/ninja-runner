﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[System.Serializable]
public class VectorEvent : UnityEvent<Vector3>
{
}
public class InputControl : MonoBehaviour {
    #region Singleton
    public static InputControl Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    public float swipedistance=10;
    public float tapdistance = 0.2f;
    public float taptime = 0.2f;
    public float doubletapdistance = 0.3f;
    public float doubletaptime = 0.3f;
    public LayerMask mask;
    public VectorEvent Swipe,Tap, Drag, Doubletap;
    public UnityEvent Release;

    Vector3 startpoint;
    Vector3 endpoint;
    float starttime;
    Camera cam;
    bool firsttap = false;
    Vector3 firsttappoint;
    bool touched=false;
	// Use this for initialization
	void Start () {
		cam= Camera.main;
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (Physics.Raycast(ray, out hit, 50, mask))
                {
                    endpoint = hit.point;
                }
                if (Input.GetMouseButtonDown(0))
                {
                    touched = true;
                    starttime = Time.time;
                    startpoint = hit.point;
                }
            }
            if (Vector3.Distance(endpoint, startpoint) > swipedistance)
            {
                if (Drag != null && touched) Drag.Invoke(endpoint - startpoint);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {

            if (Release != null) Release.Invoke();
            if (Vector3.Distance(endpoint, startpoint) > swipedistance)
            {
                if (Swipe != null && touched) Swipe.Invoke(endpoint - startpoint);
            }
            else if (Vector3.Distance(endpoint, startpoint) < tapdistance && Time.time - starttime < taptime)
            {
                if (!firsttap)
                {
                    if (Tap != null) Tap.Invoke(endpoint);
                    firsttappoint = endpoint;
                    StartCoroutine(DoubleTapWait());
                }
                else if (Doubletap != null && Vector3.Distance(firsttappoint, endpoint) < tapdistance)
                    Doubletap.Invoke(endpoint);
            }
            touched = false;
        }
    }
    
    IEnumerator DoubleTapWait()
    {
        firsttap = true;
        yield return new WaitForSeconds(doubletaptime);
        firsttap = false;
    }
}
