﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Enemy : MonoBehaviour {
    public int id;
    public int point;
    public float preparetime;
    public float attackdelay;
    public float rotspeed;
    public float movespeed;
    public float detectrange;
    public float attackrange;
    public ParticleSystem deathparticle, hitparticle;

    Animator anim;
    bool attackavailable=true;
    Collider hitbox;
    Transform target;
    Rigidbody rb;
    IEnumerator att;
    bool died=false;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        target = Character.Instance.transform;
        hitbox = GetComponent<Collider>();
        anim = GetComponent<Animator>();
	}
    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerAttack")
        {
            Die();
        }
    }
    private void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance < detectrange && !died)
        {
            Move();
        }
        else if (distance > detectrange) anim.SetBool("Walking", false);
        if (distance <= attackrange &&!died)
        {
            Vector3 targetdir = target.position - transform.position;
            targetdir.y = 0;
            Vector3 newdir = Vector3.RotateTowards(transform.forward, targetdir, rotspeed * Time.deltaTime, 0.0f);
            transform.rotation = Quaternion.LookRotation(newdir);
            if(attackavailable)
                Attack();
        }
        if (distance > attackrange&& !attackavailable)
        {

            StopCoroutine(att);
            attackavailable = true;
        }
    }
    public virtual void Move()
    {
        anim.SetBool("Walking", true);
        float distance = Vector3.Distance(target.position, transform.position);
        Vector3 targetdir = target.position - transform.position;
        targetdir.y = 0;
        Vector3 newdir = Vector3.RotateTowards(transform.forward, targetdir, rotspeed * Time.deltaTime, 0.0f);
        transform.rotation = Quaternion.LookRotation(newdir);
        if (Vector3.Angle(transform.forward, targetdir) < 2 && distance > attackrange)
            rb.position = Vector3.MoveTowards(transform.position, target.position, movespeed * Time.deltaTime);
    }
    public virtual void Attack()
    {
        att = attack();
        StartCoroutine(att);
    }
    void Die()
    {
        StartCoroutine(DieCoroutine());
    }
    IEnumerator attack()
    {
        attackavailable = false;
        anim.SetTrigger("Attack");
        yield return new WaitForSeconds(preparetime);
        hitparticle.Play();
        CameraControl.Instance.CameraShake(2);
        Character.Instance.Die();
        yield return new WaitForSeconds(attackdelay);
        attackavailable = true;
    }
    IEnumerator DieCoroutine()
    {
        died = true;
        StopCoroutine(att);
        anim.speed = 0;
        ScoreManager.Instance.KillPoint(point,id);
        CameraControl.Instance.CameraShake(1);
        ComboCounter.Instance.Count();
        hitbox.enabled = false;
        yield return new WaitForSecondsRealtime(0.3f);
        deathparticle.gameObject.transform.position = transform.position+Vector3.up;
        deathparticle.Emit(6);
        transform.position = Vector3.zero;
        died = false;
        attackavailable = true;
        hitbox.enabled = true;
        this.gameObject.SetActive(false);
    }
}
