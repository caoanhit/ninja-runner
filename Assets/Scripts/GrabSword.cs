﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabSword : MonoBehaviour {
    public Transform Sword;
    public Transform Hand;
	public void Grab()
    {
        Sword.SetParent(Hand);
        Sword.localPosition = Vector3.zero;
        Sword.localRotation = Quaternion.identity;
    }
}
