﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour {
    #region Singleton
    public static LineDrawer Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    public float maxlength=10;
    public float lenghtmul=0.5f;
    LineRenderer line;
    // Use this for initialization
    Vector3 point1, point2;
	void Start () {
        line = GetComponent<LineRenderer>();
	}
    public void DrawLine(Vector3 point)
    {
        point1 = transform.position;
        point2 = transform.position+ point.normalized*Mathf.Min(point.magnitude*lenghtmul,maxlength);
        line.SetPositions(new Vector3[] {point1,point2 });
    }
    public void StopDraw()
    {
        line.SetPositions(new Vector3[] { Vector3.zero,Vector3.zero });
    }
}
